---
layout: home
---
### Long Story Short
My name is Redon. I'm originally from Elbasan and currently based in [41°19′44″N 19°49′04″E](https://osm.org/go/xexSP~s?m=).
In 2012 I co-founded [Open Labs hackerspace](https://openlabs.cc) and started co-organizing [Open Source Conference Albania](https://oscal.openlabs.cc), CryptoParty Tirana and many other smaller and larger events focused on digital rights, the commons and free libre open source tech. I'm also a long time Wikimedia and OpenStreetMap contributor with a focus in the places where I live.

##### Professional
Currently I pay my bills as a member of [Cloud68.co](https://cloud68.co/about) helping small-medium teams and individuals move away from big tech as easy as possible.  

##### Things I enjoy doing
I enjoy growing my [record collection](https://www.youtube.com/watch?v=z3_swo25ctM) and spreading counterculture propaganda, while throwing [kung-fu kicks](https://youtu.be/yac1Q0DLJik) to [nazis](https://youtu.be/aFh08JEKDYk).

##### _Content (un)licensing_
_Unless otherwise noted all the content published here is dedicated to the [public domain](https://creativecommons.org/choose/zero/). Copyright is for the ones that get horny on gaining more capital on the shoulder of others._
